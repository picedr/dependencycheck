# Scan de dépendances (FR)


## Introduction
Le scan de dépendance permet, comme son nom l'indique, d'effectuer un scan des dépendances applicatives afin de remonter les vulnérabilités liées à celles-ci.  
L'outil proposé ici s'appuit sur [Depedency Check](https://owasp.org/www-project-dependency-check/), un outil open source proposé par [l'OWASP](https://owasp.org/).  

## Installation
Dans un premier temps, il faut verifier que les commandes suivantes sont disponibles sur votre poste (elles seront nécessaire à l'installation et au bon fonctionnement des outils) :
- unzip
- gawk

De plus, un java 8+ sera demandé pour l'éxecution de dependency check.  

Une fois ces prérequis validés, il suffit de lancer le script ***intstall.sh***.  
Celui-ci installera dependency check (accessible par la commande ***dependency-check***) et ***dcScan***.  

## Usage
***dependency-check*** permet une utilisation brute de l'outil dont la doc est [ici](https://jeremylong.github.io/DependencyCheck/dependency-check-cli/arguments.html).  
***dcScan*** permet une utilisation pré-configurée de l'outil. Il prend en paramètre le chemin du projet à scanner (ou rien si vous êtes déjà à la racine de votre projet). Il affiche un rapport et génère deux fichiers (csv et html) contenant celui-ci.

Dependency check est capable de scanner :
- Stable : Java & .NET
- Experimental :
  - CocoaPods
  - Swift Package Manager
  - Python
  - PHP (composer)
  - Node.js
  - Ruby

# Dependencies scan (EN)

## Introduction
Dependency scan let you check for vulnerabilities into your application dependencies.
The present tool is based on [Depedency Check](https://owasp.org/www-project-dependency-check/), an [OWASP](https://owasp.org/) tool.

## Installation
First of all, make sure that the following commands are available on your computer :
- unzip
- gawk

Futhermore, java 8+ will be necessary for dependency check run.  

Once all of this OK, just run ***intstall.sh***.  
It will install dependency check (launchavable with ***dependency-check*** command) and ***dcScan***.  

## Usage
***dependency-check*** let you raw use dependency check. Documentation is available [here](https://jeremylong.github.io/DependencyCheck/dependency-check-cli/arguments.html).  
***dcScan*** is a *all configured* command. It as one parameter : path of your project (no parameter if you are already into it). It displays a report and generate two files (csv and html) containing it.

Dependency check can scan :
- Fully supported: Java & .NET
- Experimental Analyzers:
  - CocoaPods
  - Swift Package Manager
  - Python
  - PHP (composer)
  - Node.js
  - Ruby
