#!/bin/sh

RED='\033[0;31m'
NC='\033[0m' # No Color

if [ -d "/usr/local/bin/dependencycheck" ]
then
  printf "Dependency check est déja intallé, voulez vous écraser ? (y,N)"
  read reponse
  if [ "${reponse}" = "y" ]
  then
    printf "${RED}Suppression de la version existante${NC}\n"
    rm -f /usr/local/bin/dependency-check
    rm -Rf /usr/local/bin/dependencycheck
  else
    echo "Annulation de l'installation"
    exit 0
  fi    
fi

if ! type "unzip"; then  
  printf "La commande ${RED}unzip${NC} est nécessaire, merci de l'installer"
  exit 1
fi

if ! type "gawk"; then
  printf "La commande ${RED}gawk${NC} est nécessaire, merci de l'installer"
  exit 1
fi

DC_VERSION=`cat dc.conf | grep VERSION | cut -d = -f2`

printf "${RED}Téléchargement de la version ${DC_VERSION}${NC}\n"
curl -LO https://dl.bintray.com/jeremy-long/owasp/dependency-check-${DC_VERSION}.zip 

printf "${RED}Installation${NC}\n"
unzip dependency-check-${DC_VERSION}.zip
cp -R dependency-check /usr/local/bin/dependencycheck
ln -s /usr/local/bin/dependencycheck/bin/dependency-check.sh /usr/local/bin/dependency-check

printf "${RED}Nettoyage${NC}\n"
rm -Rf dependency-check
rm -f dependency-check-${DC_VERSION}.zip

cp dcScan /usr/local/bin

if ! type "java"; then
  printf "Attention, JAVA ne semble pas présent sur cette machine, il sera nécessaire au bon fonctionnement de dependency check"
  exit 1
fi
